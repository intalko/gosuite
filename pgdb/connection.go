package pgdb

import (
	"context"

	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	_ "github.com/jackc/pgx/v5/pgtype"

	// pgxtype: to facilitate pgx/pgxpool with pgtype
	"github.com/jackc/pgx/v5/pgxpool"

	// stdlib: connecting migrate to Postgres via pgx
	_ "github.com/jackc/pgx/v5/stdlib"
)

type QueryExecer interface {
	Exec(ctx context.Context, sql string, arguments ...any) (commandTag pgconn.CommandTag, err error)
	Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
}
type queryExecerCtxKeyType struct{}

var QueryExecerCtxKey queryExecerCtxKeyType

type adaptedStatementBuilder struct{ sq.StatementBuilderType }

// Qb is exported squirrel query builder initiated with dollar format for convenience
var Qb adaptedStatementBuilder = adaptedStatementBuilder{sq.StatementBuilder.PlaceholderFormat(sq.Dollar)}

type ctxConn struct {
	*pgxpool.Conn
	context.Context
}
type ctxTx struct {
	pgx.Tx
	context.Context
}

type EnhancedDbPool struct{ *pgxpool.Pool }

func (p *EnhancedDbPool) extractQueryExecerFromCtx(ctx context.Context) (q QueryExecer) {
	c := ctx.Value(QueryExecerCtxKey)
	if c == nil {
		return p.Pool
	}
	if cChk, ok := c.(QueryExecer); ok {
		return cChk
	}
	return p.Pool
}

// Acquire returns *pgx.Conn wrapped together with context.Context.
// As such, there is no need to pass Conn to DB calls that might optionally be called with a Conn.
func (p *EnhancedDbPool) AcquireCtx(c context.Context) (atx *ctxConn, err error) {
	conn, err := p.Pool.Acquire(c)
	if err != nil {
		return
	}
	var a QueryExecer = conn
	ctx := context.WithValue(c, QueryExecerCtxKey, a)
	atx = &ctxConn{conn, ctx}
	return
}

// BeginCtx returns pgx.Tx wrapped together with context.Context.
// As such, there is no need to pass Tx to DB calls that might optionally be called with Tx.
func (p *EnhancedDbPool) BeginCtx(c context.Context) (atx *ctxTx, err error) {
	return p.BeginTxCtx(c, pgx.TxOptions{})
}

// BeginCtx returns pgx.Tx wrapped together with context.Context.
// As such, there is no need to pass Tx to DB calls that might optionally be called with Tx.
func (p *EnhancedDbPool) BeginTxCtx(c context.Context, t pgx.TxOptions) (atx *ctxTx, err error) {
	pTx, err := p.BeginTx(c, t)
	if err != nil {
		return
	}
	var a QueryExecer = pTx
	ctx := context.WithValue(c, QueryExecerCtxKey, a)
	atx = &ctxTx{pTx, ctx}
	return
}

// Query tries to find QueryExecer from ctx, and if not found, executes the query using its pool
func (p *EnhancedDbPool) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	q := p.extractQueryExecerFromCtx(ctx)
	return q.Query(ctx, sql, args...)
}

// QueryRow tries to find QueryExecer from ctx, and if not found, executes the query using its pool
func (p *EnhancedDbPool) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	q := p.extractQueryExecerFromCtx(ctx)
	return q.QueryRow(ctx, sql, args...)
}

// Exec tries to find QueryExecer from ctx, and if not found, executes the query using its pool
func (p *EnhancedDbPool) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	q := p.extractQueryExecerFromCtx(ctx)
	return q.Exec(ctx, sql, args...)
}

// QbQuery takes a finished Squirrel's query builder and executes the resulting query
func (p *EnhancedDbPool) QbQuery(ctx context.Context, builtQuery sq.Sqlizer) (pgx.Rows, error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return nil, err
	}
	return p.Query(ctx, q, args...)
}

// QbQueryRow takes a finished Squirrel's query builder and executes the resulting query
func (p *EnhancedDbPool) QbQueryRow(ctx context.Context, builtQuery sq.Sqlizer) (pgx.Row, error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return nil, err
	}
	return p.QueryRow(ctx, q, args...), nil
}

// QbExec takes a finished Squirrel's query builder and executes the resulting statement
func (p *EnhancedDbPool) QbExec(ctx context.Context, builtQuery sq.Sqlizer) (c pgconn.CommandTag, err error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return
	}
	return p.Exec(ctx, q, args...)
}

// DbPool is PGX pool connection, initiated by ConnectPgxPool. Do not use before the initialization.
var DbPool *EnhancedDbPool

// ConnectPgxPool initiate the exported DbPool with database connection using pgx pool connection
func ConnectPgxPool(ctx context.Context, c Config) (err error) {
	DbPool, err = c.Connect(ctx)
	return
}

// Query runs the query using the default DbPool.
func Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	return DbPool.Query(ctx, sql, args...)
}

// QueryRow runs the (single row) query using the default DbPool retrieving 1 row.
func QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	return DbPool.QueryRow(ctx, sql, args...)
}

// Exec runs the statement using the default DbPool.
func Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	return DbPool.Exec(ctx, sql, args...)
}

// QbQuery takes a finished Squirrel's query builder and executes the resulting query using the default DbPool
func QbQuery(ctx context.Context, builtQuery sq.Sqlizer) (pgx.Rows, error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return nil, err
	}
	return DbPool.Query(ctx, q, args...)
}

// QbQueryRow takes a finished Squirrel's query builder and executes the resulting query using the default DbPool
func QbQueryRow(ctx context.Context, builtQuery sq.Sqlizer) (pgx.Row, error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return nil, err
	}
	return DbPool.QueryRow(ctx, q, args...), nil
}

// QbExec takes a finished Squirrel's query builder and executes the resulting statement using the default DbPool
func QbExec(ctx context.Context, builtQuery sq.Sqlizer) (c pgconn.CommandTag, err error) {
	q, args, err := builtQuery.ToSql()
	if err != nil {
		return
	}
	return DbPool.Exec(ctx, q, args...)
}

// StdDb is pgx's stadard library SQL adaptor, initiated by ConnectStdLib. Do not use before the initialization.
var StdDb *sql.DB

// ConnectStdLib initiate the exported StdDb with database connection using pgx stdlib
func ConnectStdLib(ctx context.Context, c Config) error {
	var err error
	StdDb, err = sql.Open("pgx", c.GetDsn())
	if err != nil {
		return err
	}
	if err = StdDb.Ping(); err != nil {
		return err
	}
	go func() {
		<-ctx.Done()
		StdDb.Close()
	}()
	return nil
}
