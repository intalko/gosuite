package pgdb

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/pkg/errors"
)

type PoolParam struct {
	// Non-zero values will be passed to DbPool initialization. Defaults to 0.
	MinConns int `yaml:"MinConns" env:"pg_min_conns"`
	// Non-zero values will be passed to DbPool initialization. Defaults to max(numCPU, 4)
	MaxConns int `yaml:"MaxConns" env:"pg_max_conns"`
	// Non-zero values will be passed to DbPool initialization. Set with time units (s, m, h). Defaults to 2 mins.
	ConnectTimeout string `yaml:"ConnectTimeout" env:"pg_connect_timeout"`
	// Non-zero values will be passed to DbPool initialization. Set with time units (s, m, h). Defaults to 30 mins.
	MaxConnIdleTime string `yaml:"MaxConnIdleTime" env:"pg_max_conn_idle_time"`
	// Non-zero values will be passed to DbPool initialization. Set with time units (s, m, h). Defaults to 1 hour.
	MaxConnLifetime string `yaml:"MaxConnLifetime" env:"pg_max_conn_lifetime"`
	// Non-zero values will be passed to DbPool initialization. Set with time units (s, m, h). Defaults to 60s.
	HealthCheckPeriod string `yaml:"HealthCheckPeriod" env:"pg_health_check_period"`
}

type Config struct {
	// If set, URL will override other connection parameters
	//
	// Use this parameter to specify more advanced connection parameters such as SSL
	URL                              string    `yaml:"URL" env:"PG_URL"`
	Host                             string    `yaml:"Host" env:"PG_HOST"`
	Port                             int       `yaml:"Port" env:"PG_PORT"`
	Database                         string    `yaml:"Database" env:"PG_DATABASE"`
	Username                         string    `yaml:"Username" env:"PG_USERNAME"`
	Password                         string    `yaml:"Password" env:"PG_PASSWORD"`
	SSL                              string    `yaml:"SSL" env:"PG_SSL"`
	PoolParam                        PoolParam `yaml:"PoolParam"`
	MigrationStatementTimeoutSeconds int       `yaml:"MigrationStatementTimeoutSeconds" env:"PG_MIGRATION_STATEMENT_TIMEOUT_SECONDS"`
	MigrationFromEmbedded            bool      `yaml:"MigrationFromEmbedded" env:"PG_MIGRATION_FROM_EMBEDDED"`
	MigrationDirectory               string    `yaml:"MigrationDirectory" env:"PG_MIGRATION_DIRECTORY"`
}

func (c Config) GetDsn() string {
	if len(c.URL) > 0 {
		return c.URL
	}
	dbPort := c.Port
	dbHost := c.Host
	dbUser := c.Username
	dbPass := c.Password
	dbName := c.Database
	dbSsl := c.SSL
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s", dbHost, dbPort, dbUser, dbPass, dbName, dbSsl)
}

// Connect initialize a Config into a pgxpool.Pool, enhanced with
// context-aware Acquire/Begin and Query/Exec methods.
func (c Config) Connect(ctx context.Context) (p *EnhancedDbPool, err error) {
	pgxConfig, err := pgxpool.ParseConfig(c.GetDsn())
	if err != nil {
		return
	}
	if c.PoolParam.MinConns > 0 {
		pgxConfig.MinConns = int32(c.PoolParam.MaxConns)
	}
	if c.PoolParam.MaxConns > 1 {
		pgxConfig.MaxConns = int32(c.PoolParam.MaxConns)
	}
	var d time.Duration
	if len(c.PoolParam.ConnectTimeout) > 0 {
		d, err = time.ParseDuration(c.PoolParam.ConnectTimeout)
		if err != nil {
			return
		}
		if d < time.Second {
			err = errors.New("cannot have PoolParam.ConnectTimeout less than 1 second")
			return
		}
		pgxConfig.ConnConfig.ConnectTimeout = d
	}
	if len(c.PoolParam.MaxConnIdleTime) > 0 {
		d, err = time.ParseDuration(c.PoolParam.MaxConnIdleTime)
		if err != nil {
			return
		}
		if d < time.Second {
			err = errors.New("cannot have PoolParam.MaxConnIdleTime less than 1 second")
			return
		}
		pgxConfig.MaxConnIdleTime = d
	}
	if len(c.PoolParam.MaxConnLifetime) > 0 {
		d, err = time.ParseDuration(c.PoolParam.MaxConnLifetime)
		if err != nil {
			return
		}
		if d < time.Second {
			err = errors.New("cannot have PoolParam.MaxConnLifetime less than 1 second")
			return
		}
		pgxConfig.MaxConnLifetime = d
	}
	if len(c.PoolParam.HealthCheckPeriod) > 0 {
		d, err = time.ParseDuration(c.PoolParam.HealthCheckPeriod)
		if err != nil {
			return
		}
		if d < time.Second {
			err = errors.New("cannot have PoolParam.HealthCheckPeriod less than 1 second")
			return
		}
		pgxConfig.HealthCheckPeriod = d
	}
	dbPoolInner, err := pgxpool.NewWithConfig(ctx, pgxConfig)
	if err != nil {
		return
	}
	p = &EnhancedDbPool{dbPoolInner}
	return
}
