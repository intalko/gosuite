package pgdb

import (
	"bufio"
	"context"
	"embed"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source"

	"github.com/golang-migrate/migrate/v4/source/httpfs"
)

func createNewMigrationFiles(migrationDirPath string, migrationName string) (string, error) {
	var err error
	migrationContentTemplate := []byte("BEGIN;\n\nCOMMIT;\n")
	pwd, _ := os.Getwd()
	migrationDir := pwd + "/" + migrationDirPath
	migrationDirInfo, err := os.Stat(migrationDir)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(migrationDir, 0755)
			if err != nil {
				return "", fmt.Errorf("migrations directory not exists and creation failed: %s", err)
			}
			migrationDirInfo, err = os.Stat(migrationDir)
			if err != nil {
				return "", fmt.Errorf("migrations directory not exists and creation failed: %s", err)
			}
		} else {
			return "", fmt.Errorf("stat migration directory failed: %s", err)
		}
	}
	if !migrationDirInfo.IsDir() {
		return "", fmt.Errorf("path %s is not a directory", migrationDir)
	}
	datePrefix := createMigrationTimestamp()
	migrationFileName := migrationDir + "/" + datePrefix + "_" + migrationName
	if err = os.WriteFile(migrationFileName+".up.sql", migrationContentTemplate, 0777); err != nil {
		return "", err
	}
	if err = os.WriteFile(migrationFileName+".down.sql", migrationContentTemplate, 0777); err != nil {
		return "", err
	}
	msg := "Created blank migrations: \n"
	msg += "\t" + migrationDirPath + "/" + datePrefix + "_" + migrationName + ".down.sql\n"
	msg += "\t" + migrationDirPath + "/" + datePrefix + "_" + migrationName + ".up.sql\n"
	return msg, nil
}

func createMigrationTimestamp() string {
	t := time.Now()
	return fmt.Sprintf(
		"%d%02d%02d%02d%02d%02d",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
		t.Second())
}

func logMigrateStart() {
	log.Println("Starting migration...")
}

// MigrateEntryPoint is the entrypoint for DB migration commands.
// If config.MigrationFromEmbedded is true, the (optional)
// embedded migration directory must then be set (only 1 is accepted)
//
// The simplest way to include the embed:
//
//	import "embed"
//	//go:embed migrations
//	var embeddedMigrations embed.FS
func MigrateEntryPoint(config Config, optionally1EmbeddedDirs ...embed.FS) {
	flag.Parse()
	err := migrateCliSystem(flag.Args(), config, optionally1EmbeddedDirs...)
	if err != nil {
		log.Fatalln("Error:", err)
	} else {
		log.Println("Migration completed successfully")
		os.Exit(0)
	}
}

func migrateCliSystem(fullArgs []string, c Config, embeddedDirs ...embed.FS) (err error) {
	migrateSubCommand := flag.NewFlagSet("migrate", flag.ContinueOnError)
	if err := migrateSubCommand.Parse(fullArgs[1:]); err != nil {
		return fmt.Errorf("error parsing arguments: %s", err)
	}
	migrateArgs := migrateSubCommand.Args()
	argsLength := len(migrateArgs)
	if argsLength < 1 {
		return errors.New("migrate subcommand not specified")
	}

	if migrateArgs[0] == "create" {
		if argsLength != 2 {
			return fmt.Errorf("migrate create called without specifying exactly the name for the migration")
		}
		if c.MigrationFromEmbedded {
			return errors.New("called create migrate command when migration config has MigrationFromEmbedded")
		}
		logMigrateStart()
		msg, err := createNewMigrationFiles(c.MigrationDirectory, migrateArgs[1])
		if err != nil {
			return fmt.Errorf("failed to create new migration files %s", err)
		}
		log.Println(msg)
		return nil
	}
	if migrateArgs[0] == "list" {
		if argsLength != 1 {
			return fmt.Errorf("migrate list called with extra commands")
		}
		logMigrateStart()
		var sourceFs fs.FS
		ctx := context.Background()
		sourceFs, err = MigrateGetSourceFs(ctx, c, embeddedDirs...)
		if err != nil {
			return
		}
		var files []fs.DirEntry
		files, err = fs.ReadDir(sourceFs, ".")
		if err != nil {
			return
		}
		sort.Slice(files, func(i, j int) bool {
			return files[i].Name() < files[j].Name()
		})
		var sourceDriver source.Driver
		sourceDriver, err = migrateSourceFsToSourceInstance(sourceFs)
		var dbDriver database.Driver
		dbDriver, err = MigrateGetDbDriver(ctx, c)
		var migrateInstance *migrate.Migrate
		migrateInstance, err = migrateMkInstanceFromSourceAndDriver(ctx, sourceDriver, dbDriver)
		var v uint
		var dirty bool
		v, dirty, err = migrateInstance.Version()
		msg := "Migration file entries:\n"
		dirtyPostfix := ""
		for i, file := range files {
			fileName := file.Name()
			if !strings.HasSuffix(fileName, "down.sql") && !strings.HasSuffix(fileName, "up.sql") {
				continue
			}
			markerPrefix := "  -  "
			dirtyPostfix = ""
			if strings.HasPrefix(fileName, fmt.Sprint(v)) && strings.HasSuffix(fileName, "up.sql") {
				markerPrefix = "  X  "
				if dirty {
					dirtyPostfix = "[DIRTY] "
				}
			}
			if i == 0 && v == 0 && strings.HasSuffix(fileName, "down.sql") {
				markerPrefix = "  X  "
			}
			msg += fmt.Sprintf("%s%s%s\n", markerPrefix, dirtyPostfix, fileName)
		}
		log.Println(msg)
		return nil
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var migrationInstance *migrate.Migrate
	migrationInstance, err = MigrateGetInstance(ctx, c, embeddedDirs...)
	if err != nil {
		return fmt.Errorf("migration instance failed to initialize: %s", err)
	}

	sigIntChan := make(chan os.Signal, 1)
	signal.Notify(sigIntChan, syscall.SIGINT)
	go func() {
		<-sigIntChan
		log.Println("Received Ctrl+C")
		cancel()
	}()
	switch migrateArgs[0] {
	case "down":
		nStep := -1
		if argsLength > 2 {
			return fmt.Errorf("migrate down is called with extra arguments")
		}
		if argsLength == 2 {
			parsedSteps, err := strconv.ParseInt(migrateArgs[1], 10, 64)
			if err != nil || parsedSteps < 1 {
				return fmt.Errorf("migrate down is called not with legible integer")
			}
			nStep = int(parsedSteps * -1)
		}
		logMigrateStart()
		log.Printf("Executing %d down migration steps\n", nStep*-1)
		if err = migrationInstance.Steps(nStep); err != nil {
			return fmt.Errorf("migration execution error: %v", err)
		}
	case "up":
		nStep := 1
		if argsLength > 2 {
			return fmt.Errorf("migrate up is called with extra arguments")
		}
		if argsLength == 2 {
			parsedSteps, err := strconv.ParseInt(migrateArgs[1], 10, 64)
			if err != nil || parsedSteps < 1 {
				return fmt.Errorf("migrate up is called not with legible integer")
			}
			nStep = int(parsedSteps)
		}
		logMigrateStart()
		log.Printf("Executing %d up migration steps\n", nStep)
		if err = migrationInstance.Steps(nStep); err != nil {
			return fmt.Errorf("migration execution error: %v", err)
		}
	case "force":
		if argsLength != 2 {
			return fmt.Errorf("migrate force is called without specifying exactly the version to revert to")
		}
		destinationVersion, err := strconv.ParseInt(migrateArgs[1], 10, 64)
		if err != nil {
			return fmt.Errorf("failed to parse version to revert to: %v", err)
		}
		logMigrateStart()
		err = migrationInstance.Force(int(destinationVersion))
		if err != nil {
			return fmt.Errorf("failed to force migration versions: %v", err)
		}
	case "latest":
		if argsLength != 1 {
			return fmt.Errorf("migrate latest is called with extra arguments")
		}
		logMigrateStart()
		err = migrationInstance.Up()
		if err != nil {
			return fmt.Errorf("migrate latest errorred: %v", err)
		}
	case "drop":
		if argsLength != 1 {
			return fmt.Errorf("migrate drop is called with extra arguments")
		}
		logMigrateStart()
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Print("This will drop EVERYTHING in the database. Proceed? (y/n): ")
		scanner.Scan()
		reply := strings.ToLower(scanner.Text())
		if reply == "y" || reply == "yes" {

			dropErr := migrationInstance.Drop()
			if dropErr != nil {
				return fmt.Errorf("failed to drop database contents")
			}
			return nil
		}
		return fmt.Errorf("no confirmation, database drop cancelled")
	case "version":
		v, dirty, err := migrationInstance.Version()
		if err != nil {
			return fmt.Errorf("getting migration version failed: %s", err)
		}
		dirtyText := ""
		if dirty {
			dirtyText = " [DIRTY]"
		}
		log.Printf("Current Version: %d%s", v, dirtyText)
		return nil
	default:
		return fmt.Errorf("migration subcommand entered is not supported: %s", migrateArgs[0])
	}
	return nil
}

// MigrateGetSourceFs exposes the lower-level migration integration
func MigrateGetSourceFs(ctx context.Context, c Config, embeddedDirs ...embed.FS) (migrationFS fs.FS, err error) {
	if c.MigrationFromEmbedded {
		if len(embeddedDirs) < 1 {
			err = errors.New("migration has config.MigrationFromEmbedded enabled when no embedded migration directory is supplied")
			return
		}
		if len(embeddedDirs) > 1 {
			err = fmt.Errorf("expected exactly 1 embedded migration directory, found: %d", len(embeddedDirs))
			return
		}

		migrationFS = embeddedDirs[0]
		migrationFS, err = fs.Sub(migrationFS, c.MigrationDirectory)
		if err != nil {
			err = fmt.Errorf("directory %s within embedded FS cannot be opened", c.MigrationDirectory)
			return
		}

	} else {
		var migrationDir *os.File
		migrationDir, err = os.OpenFile(c.MigrationDirectory, os.O_RDONLY, 0755)
		go func() {
			<-ctx.Done()
			migrationDir.Close()
			os.Exit(0)
		}()
		if err != nil {
			err = errors.New("error opening migrations directory")
			return
		}
		var migrationDirStat fs.FileInfo
		migrationDirStat, err = migrationDir.Stat()
		if err != nil {
			err = fmt.Errorf("stat migration directory failed: %s", err)
			return
		}
		if !migrationDirStat.IsDir() {
			err = fmt.Errorf("not a directory: the path %s", c.MigrationDirectory)
			return
		}
		migrationFS = os.DirFS(c.MigrationDirectory)
	}
	return
}

func migrateSourceFsToSourceInstance(migrationFS fs.FS) (migrationSourceDriver source.Driver, err error) {
	migrationSourceDriver, err = httpfs.New(http.FS(migrationFS), ".")
	if err != nil {
		err = fmt.Errorf("reading httpfs as migration source instance failed: %s", err)
	}
	return
}
func migrateMkInstanceFromSourceAndDriver(ctx context.Context, migrationSourceDriver source.Driver, migrateDbDriver database.Driver) (migrateInstance *migrate.Migrate, err error) {
	migrateInstance, err = migrate.NewWithInstance(
		"httpfs",
		migrationSourceDriver,
		"postgres",
		migrateDbDriver,
	)
	if err != nil {
		go func() {
			<-ctx.Done()
			migrateInstance.GracefulStop <- true
		}()
	}
	return
}

// MigrateGetSource exposes the lower-level migration integration
func MigrateGetSource(ctx context.Context, c Config, embeddedDirs ...embed.FS) (migrationSourceDriver source.Driver, err error) {
	var migrationFS fs.FS
	migrationFS, err = MigrateGetSourceFs(ctx, c, embeddedDirs...)
	if err != nil {
		return
	}
	return migrateSourceFsToSourceInstance(migrationFS)
}

// MigrateGetDbDriver exposes the lower-level migration integration
func MigrateGetDbDriver(ctx context.Context, c Config) (pgDatabaseDriver database.Driver, err error) {
	if err = ConnectStdLib(ctx, c); err != nil {
		err = fmt.Errorf("failed to connect to database: %v", err)
		return
	}
	pgDatabaseDriver, err = postgres.WithInstance(StdDb, &postgres.Config{
		DatabaseName:     c.Database,
		StatementTimeout: time.Duration(c.MigrationStatementTimeoutSeconds) * time.Second,
	})
	return
}

// MigrateGetInstance exposes the lower-level migration integration
func MigrateGetInstance(ctx context.Context, c Config, embeddedDirs ...embed.FS) (migrateInstance *migrate.Migrate, err error) {
	var migrateSource source.Driver
	migrateSource, err = MigrateGetSource(ctx, c, embeddedDirs...)
	if err != nil {
		return
	}
	var migrateDriver database.Driver
	migrateDriver, err = MigrateGetDbDriver(ctx, c)
	if err != nil {
		return
	}
	return migrateMkInstanceFromSourceAndDriver(ctx, migrateSource, migrateDriver)
}
