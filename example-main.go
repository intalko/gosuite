// main WON'T WORK, it's provided as example of how to use packages provided by gosuite
package main

import (
	"context"
	"embed"
	"fmt"
	"log"
	"time"

	"gitlab.com/intalko/gosuite/logger"
	"gitlab.com/intalko/gosuite/mainhelper"
	"gitlab.com/intalko/gosuite/pgdb"
)

// embedded migrations, should be the same name as supplied in pgdb.Config.MigrationDirectory
// pass this to the MigrateEntryPoint's second argument
//
//go:embed migrations
var migrationsEmbeddedDir embed.FS

func main() {
	MainExample()
}

// MainExample is an example of main function
// that uses all subpackages provided
func MainExample() {
	// configuration (mainhelper)
	//
	// Config for each subpackages can easily
	// be included as separatej struct fields
	var config struct {
		Logging  logger.Config `yaml:"Logging"`
		Postgres pgdb.Config   `yaml:"Postgres"`
	}
	// defined configuration is easily scanned
	// using GetConfig scans to app defined
	// interface{}
	err := mainhelper.GetConfig(&config)
	if err != nil {
		log.Fatalln("getting config failed")
		return
	} else {
		log.Println("Logging config: ", config.Logging)
		log.Println("Postgres config: ", config.Postgres)
	}

	ctx := context.Background()

	// logging (logger)
	err = logger.InitLogger(ctx, config.Logging)
	if err != nil {
		log.Fatalf("logger initiation has errors: %s", err)
		return
	}

	// cli functionalities handling (mainhelper)
	cliFn, hasCliFn := mainhelper.SubcommandEntryPoint(mainhelper.StringFuncMap{
		"migrate": // using the MigrateEntryPoint is most easily integrated this way
		func() {
			pgdb.MigrateEntryPoint(config.Postgres, migrationsEmbeddedDir)
		},
		"logtest": // log some gibberish as test
		func() {
			simpleLoggingTest()
		},
	})
	if hasCliFn {
		cliFn()
		return
	}
}

func simpleLoggingTest() {
	now := time.Now()
	nowStr := now.Format(time.RFC3339)
	logger.Logger.Log().Msg("NoSeverityHere, Entree 0")
	logger.Info().Interface("testpayload_time_of_now", now).Msg(fmt.Sprint("Entree 1: ", nowStr))
	time.Sleep(time.Duration(2) * time.Second)
	now = time.Now()
	nowStr = now.Format(time.RFC3339)
	logger.Info().Interface("testpayload_time_of_next", now).Msg(fmt.Sprint("Entree 2: ", nowStr))
	now = time.Now()
	nowStr = now.Format(time.RFC3339)
	time.Sleep(time.Duration(2) * time.Second)
	logger.Debug().Interface("testpayload_time_of_debug", now).Msg(fmt.Sprint("Entree 3: ", nowStr))
}
