package mainhelper

import (
	"flag"
)

type StringFuncMap = FuncMap
type FuncMap map[string]func()

// SubcommandEntryPoint accepts a map of CLI subcommands available
// to the main program, allowing decluttering of the main() entrypoint
//
// The functions should be constructed the way main() function is developed:
// accepts no parameter and handles all errors
func SubcommandEntryPoint(funcMap FuncMap) (fn func(), hasSubcommand bool) {
	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		return
	}
	fn, hasSubcommand = funcMap[args[0]]
	return
}
