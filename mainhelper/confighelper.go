package mainhelper

import (
	"context"
	"flag"
	"os"

	"github.com/pkg/errors"
	"github.com/sethvargo/go-envconfig"
	"gopkg.in/yaml.v3"
)

// GetConfigFromYaml scans config.yaml into configDest.
// Note that the configDest should be a pointer to a struct
// and the the program entrypoint has string flag "config" defined to point to
// an alternate config file.
func GetConfigFromYaml(configDest interface{}) (err error) {
	configFilename := flag.String("config", "config.yaml", "use a config file other than config.yaml in current directory")
	flag.Parse()
	file, err := os.Open(*configFilename)
	if err != nil {
		return
	}
	yamlDecoder := yaml.NewDecoder(file)
	err = yamlDecoder.Decode(configDest)
	return
}

// GetConfig combines GetConfigFromYaml and GetConfigFromEnvironment.
// config.yaml gets processed first, so if yaml sets the value first,
// the "winner" is yaml where env struct tag _overwrite_ is not set.
// Nonexisting config.yaml is ignored as configuration can be also be
// found from environment variables. Refer to both GotConfigs'
// function documentation for better reference.
func GetConfig(configDest interface{}) (err error) {
	yamlErr := GetConfigFromYaml(configDest)
	envErr := GetConfigFromEnvironment(configDest)
	if yamlErr != nil {
		if !os.IsNotExist(yamlErr) {
			err = errors.Wrap(yamlErr, "yaml parse error")
		}
	}
	if envErr != nil {
		if err != nil {
			err = errors.Wrap(envErr, err.Error()+"; env parse error")
		} else {
			err = errors.Wrap(envErr, "env parse error")
		}
	}
	return
}

// GetConfigFromEnvironment uses the excellent "github.com/sethvargo/go-envconfig"
// that sets the field when it has its zero value (unless the env struct has _overwrite_ flag set).
// The library will recurse into deeper / nested structs to set _env_ struct tags.
func GetConfigFromEnvironment(configDest interface{}) (err error) {
	err = envconfig.Process(context.Background(), configDest)
	return
}
