package logger

import (
	"github.com/rs/zerolog"
)

type cronLoggerType struct {
	// if destination is not set, it will default to writing to DefaultCronLogger
	destination *zerolog.Logger
}

// NewCronLogger accepts a custom zerolog logger and
// adapts it to a cron.Logger (github.com/robfig/cron/v3)
//
// The rationale of providing an adaptor to the excellent
// cron is to provide a levelled and structured logging
// to the cron, since the cron's logger Info and Error
// interface is more akin to non structured loggers.
// This provides automatic recognition of field key
// if the even indexed elements is string, helping
// transforming it to a map[string]interface{}
// passed to zerolog.Logger as Interface()
func NewCronLogger(logger *zerolog.Logger) *cronLoggerType {
	return &cronLoggerType{logger}
}

// Info level logging with field extraction through a map[string]interface
func (c cronLoggerType) Info(msg string, fields ...interface{}) {
	logger := c.destination
	if logger == nil {
		logger = &Logger
	}
	logEntry := logger.Info()
	sendFields(logEntry, msg, fields...)
}

// Error level logging with field extraction through a map[string]interface
func (c cronLoggerType) Error(err error, msg string, fields ...interface{}) {
	logger := c.destination
	if logger == nil {
		logger = &Logger
	}
	logEntry := logger.Info().Err(err)
	sendFields(logEntry, msg, fields...)
}

// zerolog's Fields method is not used
// as it discards extra/odd and
// non-string fields
func sendFields(l *zerolog.Event, msg string, fields ...interface{}) {
	fieldsLength := len(fields)
	if fieldsLength == 0 {
		return
	}
	extraFields := make([]interface{}, 0, fieldsLength)
	for i := 0; i < fieldsLength/2; i++ {
		key := fields[i]
		value := fields[i+1]
		if strKey, ok := key.(string); ok {
			l = l.Interface(strKey, value)
		} else {
			extraFields = append(extraFields, key, value)
		}
	}
	if fieldsLength%2 != 0 {
		extraFields = append(extraFields, fields[fieldsLength-1])
	}
	if len(extraFields) > 0 {
		l = l.Interface("extras", extraFields)
	}
	l.Msg(msg)
}

var DefaultCronLogger cronLoggerType
