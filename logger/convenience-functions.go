package logger

import "github.com/rs/zerolog"

func Trace() *zerolog.Event {
	return Logger.Trace()
}
func Debug() *zerolog.Event {
	return Logger.Debug()
}
func Info() *zerolog.Event {
	return Logger.Info()
}
func Warn() *zerolog.Event {
	return Logger.Warn()
}
func Error() *zerolog.Event {
	return Logger.Error()
}
func Fatal() *zerolog.Event {
	return Logger.Fatal()
}
func Panic() *zerolog.Event {
	return Logger.Panic()
}
