package logger

import (
	"bytes"
	vanillaJSON "encoding/json"
	"testing"
	"time"

	goccyJSON "github.com/goccy/go-json"
	"github.com/rs/zerolog"
)

func BenchmarkZerolog(b *testing.B) {
	b.StopTimer()
	buf := bytes.NewBuffer(make([]byte, 1000))
	logger := zerolog.New(buf)
	l := logger.With().Timestamp().Logger()
	b.StartTimer()
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		l.Info().Bool("true", true).Time("now", time.Now()).Int("int", 10).Float32("float", 1.2341234).Msg("testing")
	}
	b.StopTimer()
	b.ReportAllocs()
}

func BenchmarkGoccyLoggingTyped(b *testing.B) {
	b.StopTimer()
	buf := bytes.NewBuffer(make([]byte, 1000))
	logger := zerolog.New(buf)
	zerolog.InterfaceMarshalFunc = goccyJSON.Marshal
	l := logger.With().Timestamp().Logger()
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		l.Info().Interface("true", true).Interface("now", time.Now()).Interface("int", 10).Interface("float", 1.2341234).Msg("testing")
	}
	b.StopTimer()
	b.ReportAllocs()
}
func BenchmarkVanillaLoggingTyped(b *testing.B) {
	b.StopTimer()
	buf := bytes.NewBuffer(make([]byte, 1000))
	logger := zerolog.New(buf)
	zerolog.InterfaceMarshalFunc = vanillaJSON.Marshal
	l := logger.With().Timestamp().Logger()
	b.StartTimer()
	for n := 0; n < b.N; n++ {

		l.Info().Interface("true", true).Interface("now", time.Now()).Interface("int", 10).Interface("float", 1.2341234).Msg("testing")
	}
	b.StopTimer()
	b.ReportAllocs()
}
func mkTestStruct() interface{} {
	s := struct {
		True  bool      `json:"True"`
		Time  time.Time `json:"Time"`
		Int   int       `json:"Int"`
		Float float32   `json:"Float"`
	}{
		True:  true,
		Time:  time.Now(),
		Int:   10,
		Float: 1.2341234,
	}
	return s
}
func BenchmarkGoccyLoggingStruct(b *testing.B) {
	b.StopTimer()
	s := mkTestStruct()
	buf := bytes.NewBuffer(make([]byte, 1000))
	logger := zerolog.New(buf)
	zerolog.InterfaceMarshalFunc = goccyJSON.Marshal
	l := logger.With().Timestamp().Logger()
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		l.Info().Interface("s", s).Msg("testing")
	}
	b.StopTimer()
	b.ReportAllocs()
}
func BenchmarkVanillaLoggingStruct(b *testing.B) {
	b.StopTimer()
	s := mkTestStruct()
	buf := bytes.NewBuffer(make([]byte, 1000))
	logger := zerolog.New(buf)
	zerolog.InterfaceMarshalFunc = vanillaJSON.Marshal
	l := logger.With().Timestamp().Logger()
	b.StartTimer()
	for n := 0; n < b.N; n++ {

		l.Info().Interface("s", s).Msg("testing")
	}
	b.StopTimer()
	b.ReportAllocs()
}
