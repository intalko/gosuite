package logger

import (
	"context"
	"encoding/json"
	"errors"

	"cloud.google.com/go/logging"
	"github.com/rs/zerolog"
	"google.golang.org/api/option"
)

type GcpLoggingWriter struct {
	ctx       context.Context
	wroteOnce bool
	logger    *logging.Logger

	zerolog.LevelWriter
}

type GcpLoggingSeverityMapType map[zerolog.Level]logging.Severity

// GcpLoggingSeverityMap contains the default zerolog.Level -> logging.Severity mappings.
var GcpLoggingSeverityMap = GcpLoggingSeverityMapType{
	zerolog.TraceLevel: logging.Debug,
	zerolog.DebugLevel: logging.Debug,
	zerolog.InfoLevel:  logging.Info,
	zerolog.WarnLevel:  logging.Warning,
	zerolog.ErrorLevel: logging.Error,
	zerolog.FatalLevel: logging.Critical,
	zerolog.PanicLevel: logging.Critical,
}

type GcpLoggingConfig struct {
	ProjectId              string            `yaml:"ProjectId" env:"GCPLOGGING_PROJECTID"`
	LogId                  string            `yaml:"LogId" env:"GCPLOGGING_LOGID"`
	ServiceAccountFilePath string            `yaml:"ServiceAccountFilePath" env:"GOOGLE_APPLICATION_CREDENTIALS"`
	ServiceAccountJSON     string            `yaml:"ServiceAccountJSON" env:"GCPLOGGING_SERVICEACCOUNTJSON"`
	DefaultLabels          map[string]string `yaml:"DefaultLabels" env:"GCPLOGGING_DEFAULTLABELS"`
}

func (c *GcpLoggingWriter) Write(p []byte) (int, error) {
	entry := logging.Entry{Payload: json.RawMessage(p)}
	if !c.wroteOnce {
		c.wroteOnce = true
		err := c.logger.LogSync(c.ctx, entry)
		if err != nil {
			return 0, err
		}
	} else {
		c.logger.Log(entry)
	}
	return len(p), nil
}

func (c *GcpLoggingWriter) WriteLevel(level zerolog.Level, payload []byte) (int, error) {
	entry := logging.Entry{
		Severity: GcpLoggingSeverityMap[level],
		Payload:  json.RawMessage(payload),
	}
	if !c.wroteOnce {
		c.wroteOnce = true
		err := c.logger.LogSync(c.ctx, entry)
		if err != nil {
			return 0, err
		}
	} else {
		c.logger.Log(entry)
	}
	if level == zerolog.FatalLevel {
		// ensure that any pending logs are written before exit
		err := c.logger.Flush()
		if err != nil {
			return 0, err
		}
	}
	return len(payload), nil
}

func NewGcpLoggingWriter(ctx context.Context, config GcpLoggingConfig, gcpLogOptions ...logging.LoggerOption) (writer zerolog.LevelWriter, err error) {
	if config.ProjectId == "" {
		err = errors.New("not set: GcpLoggingConfig.ProjectId")
		return
	}
	if config.LogId == "" {
		err = errors.New("not set: GcpLoggingConfig.LogId")
		return
	}
	var gcpLogClient *logging.Client
	clientOptions := make([]option.ClientOption, 0, 1)
	if config.ServiceAccountFilePath != "" && config.ServiceAccountJSON == "" {
		clientOptions = append(clientOptions, option.WithCredentialsFile(config.ServiceAccountFilePath))
	}
	if config.ServiceAccountJSON != "" {
		clientOptions = append(clientOptions, option.WithCredentialsJSON([]byte(config.ServiceAccountJSON)))
	}
	gcpLogClient, err = logging.NewClient(ctx, config.ProjectId, clientOptions...)
	if err != nil {
		return
	}
	if config.DefaultLabels != nil && len(config.DefaultLabels) > 0 {
		gcpLogOptions = append(gcpLogOptions, logging.CommonLabels(config.DefaultLabels))
	}
	logger := gcpLogClient.Logger(config.LogId, gcpLogOptions...)
	writer = &GcpLoggingWriter{
		ctx:    ctx,
		logger: logger,
	}
	return
}
