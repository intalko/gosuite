package logger

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/pkg/errors"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/journald"
	"github.com/rs/zerolog/log"
)

type Config struct {
	Destinations      outputDestinationsSetting `yaml:"Destinations" env:"LOG_DESTINATIONS"`
	GcpLoggingConfig  GcpLoggingConfig          `yaml:"GcpLoggingConfig"`
	RotatedFileConfig RotatedFilewriterConfig   `yaml:"RotatedFileConfig"`
}
type outputDestinationsSetting struct {
	Console     bool `yaml:"Console"`
	DevConsole  bool `yaml:"DevConsole"`
	GcpLogging  bool `yaml:"GcpLogging"`
	RotatedFile bool `yaml:"RotatedFile"`
	Journald    bool `yaml:"Journald"`
}

func (c *outputDestinationsSetting) UnmarshalText(b []byte) (err error) {
	s := strings.ToLower(string(b))
	if s == "" {
		return
	}
	c.Console = strings.Contains(s, "console")
	c.DevConsole = strings.Contains(s, "devconsole")
	c.GcpLogging = strings.Contains(s, "gcplogging")
	c.RotatedFile = strings.Contains(s, "rotate")
	c.Journald = strings.Contains(s, "journald")
	return
}

// Logger is the default zerolog.Logger in this package.
// It should be initialized with InitLoggerr, otherwise it defaults to
// zerolog's standard default logger
//
// It is used in this package's convenience functions like Info(), Error()
var Logger zerolog.Logger = log.Logger

type leveledWriter struct {
	logger *zerolog.Logger
	level  zerolog.Level
}

func (w leveledWriter) Write(p []byte) (n int, err error) {
	w.logger.WithLevel(w.level).Msg(string(p))
	n = len(p)
	return
}

// LeveledWriter creates a io.Writer that writes to the log
// with the specified severity and the passed bytes as Msg
//
// This makes it compatible with stdlib's Log Writer, while
// maintaining a severity level consistent with other logs
func LeveledWriter(logger *zerolog.Logger, severity zerolog.Level) leveledWriter {
	return leveledWriter{logger, severity}
}

// CreateLogger initializes the loggers in accordance to config
func (config *Config) CreateLogger(ctx context.Context) (logger zerolog.Logger, err error) {
	erroredWriters := make([]error, 0, 5)
	logWriters := make([]io.Writer, 0, 5)
	if config.Destinations.Console && !config.Destinations.DevConsole {
		logWriters = append(logWriters, os.Stdout)
	}
	if config.Destinations.DevConsole {
		consoleWriter := zerolog.NewConsoleWriter(
			func(w *zerolog.ConsoleWriter) {
				w.TimeFormat = ("060102-150405")
			},
		)
		logWriters = append(logWriters, consoleWriter)
	}
	if config.Destinations.GcpLogging {
		gcpWriter, err := NewGcpLoggingWriter(ctx, config.GcpLoggingConfig)
		if err != nil {
			erroredWriters = append(erroredWriters, errors.Wrap(err, "gcpLoggingWriter"))
		} else {
			logWriters = append(logWriters, gcpWriter)
		}
	}
	if config.Destinations.RotatedFile {
		err := InitRotatedfileWriterRotation(
			ctx,
			config.RotatedFileConfig,
		)
		if err != nil {
			erroredWriters = append(erroredWriters, errors.Wrap(err, "dailyfileLogWriter"))
		} else {
			logWriters = append(logWriters, &rotatedFileLogWriter)
		}
	}
	if config.Destinations.Journald {
		w := journald.NewJournalDWriter()
		logWriters = append(logWriters, w)
	}
	if len(erroredWriters) > 0 {
		err = fmt.Errorf("logger:initiation:writer:errors: %v", erroredWriters)
		return
	}
	logger = zerolog.New(zerolog.MultiLevelWriter(logWriters...)).With().Timestamp().Logger()
	return
}

// InitLogger initializes the loggers in accordance to the configuration passed,
// which initiates the default exported Logger
//
// While the configuration can be passed into this function, this function is intended to
// help configure a logger quickly from YAML based configuration.
//
// refer tho the repo root's example.config.yaml for reference.
func InitLogger(ctx context.Context, config Config) (err error) {
	logger, err := config.CreateLogger(ctx)
	if err != nil {
		return
	}
	Logger = logger
	return nil
}
