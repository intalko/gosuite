package logger

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/docker/go-units"
	"github.com/robfig/cron/v3"
)

type RotatedFilewriterConfig struct {
	FolderPath          string `yaml:"FolderPath" env:"LOGROTATE_FOLDER_PATH"`
	RotateCronFrequency string `yaml:"RotateCronFrequency" env:"LOGROTATE_ROTATE_CRON_FREQUENCY"`
	FileNameFormat      string `yaml:"FileNameFormat" env:"LOGROTATE_FILE_NAME_FORMAT"`
	MaxLogFilesKept     int    `yaml:"MaxLogFilesKept" env:"LOGROTATE_MAX_LOG_FILES_KEPT"`
	MinLogFilesKept     int    `yaml:"MinLogFilesKept" env:"LOGROTATE_MIN_LOG_FILES_KEPT"`
	MaxTotalLogFileSize string `yaml:"MaxTotalLogFileSize" env:"LOGROTATE_MAX_TOTAL_LOG_FILE_SIZE"`
}

var currentRotatedLogFile *os.File

// WriterStructWithDefaultDestination provides a way to change the destination io.Writer
// while maintaining the pointer to the struct itself, allowing the log's destination
// writer to be changed without reinitiating the log itself.
//
// Intended to be used with constructs like file rotation
type WriterStructWithDefaultDestination struct {
	// when set, will write to the destination
	// otherwise, will write to the default package rotatedLogFile
	Destination                  io.Writer
	closerOnContextDoneInitiated bool
	ctx                          context.Context
}

func (w *WriterStructWithDefaultDestination) Write(p []byte) (int, error) {
	if w.Destination != nil {
		return w.Destination.Write(p)
	}
	if currentRotatedLogFile == nil {
		return 0, errors.New("current rotated log file not initiated")
	}
	return currentRotatedLogFile.Write(p)
}

// SetOutput changes the destination. If the destination is io.WriteCloser, it will be closed before reassignment
func (w *WriterStructWithDefaultDestination) SetOutput(output io.Writer) {
	if outputCloser, ok := w.Destination.(io.WriteCloser); ok && w.Destination != output {
		outputCloser.Close()
	}
	w.Destination = output
}
func (w *WriterStructWithDefaultDestination) initCloseOnContextDone() {
	if outputCloser, ok := w.Destination.(io.WriteCloser); ok && !w.closerOnContextDoneInitiated {
		go func() {
			<-w.ctx.Done()
			outputCloser.Close()
		}()
		w.closerOnContextDoneInitiated = true
	}
}

var rotatedFileLogWriter WriterStructWithDefaultDestination = WriterStructWithDefaultDestination{}

func (config *RotatedFilewriterConfig) removeOldFiles() (err error) {
	var path string
	path, err = config.getAbsFolderPath()
	if err != nil {
		return
	}
	var files []fs.FileInfo
	files, err = ioutil.ReadDir(path)
	if err != nil {
		return
	}
	var relevantFilesTotalSize int64
	relevantFiles := make([]os.FileInfo, 0, len(files))
	for _, file := range files {
		_, err := time.Parse(config.FileNameFormat, file.Name())
		if err == nil {
			relevantFiles = append(relevantFiles, file)
			relevantFilesTotalSize = relevantFilesTotalSize + file.Size()
		}
	}
	sort.Slice(relevantFiles, func(i, j int) bool {
		iTs, _ := time.Parse(config.FileNameFormat, relevantFiles[i].Name())
		jTs, _ := time.Parse(config.FileNameFormat, relevantFiles[j].Name())
		return iTs.Before(jTs)
	})
	totalFilesCount := len(relevantFiles)
	maxTotalFileSize, _ := units.FromHumanSize(config.MaxTotalLogFileSize)
	if totalFilesCount >= config.MinLogFilesKept {
		return
	}
	for i := 0; i < totalFilesCount-config.MinLogFilesKept; i++ {
		file := relevantFiles[i]
		if i < (totalFilesCount-config.MaxLogFilesKept) ||
			((relevantFilesTotalSize-file.Size()) >= int64(maxTotalFileSize) &&
				maxTotalFileSize != 0) {
			relevantFilesTotalSize = relevantFilesTotalSize - file.Size()
			os.Remove(fmt.Sprintf("%s/%s", path, file.Name()))
		}
	}
	return nil
}
func (config *RotatedFilewriterConfig) getAbsFolderPath() (logsFolderPath string, err error) {
	if strings.HasPrefix(logsFolderPath, "/") {
		logsFolderPath = config.FolderPath
	} else {
		var wd string
		wd, err = os.Getwd()
		if err != nil {
			return
		}
		logsFolderPath = wd + "/" + config.FolderPath
	}
	logsDirFileInfo, err := os.Stat(logsFolderPath)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(logsFolderPath, 0755)
			if err != nil {
				err = fmt.Errorf("logs directory creation not exist and creation failed: %s", err)
				return
			}
		}
	} else {
		if !logsDirFileInfo.Mode().IsDir() {
			err = fmt.Errorf("logs path is not a directory: %s", err)
			return
		}
	}
	return
}

func initOrRotateWriterDest(ctx context.Context, config *RotatedFilewriterConfig, destination ...*WriterStructWithDefaultDestination) (err error) {
	logsFolderPath, err := config.getAbsFolderPath()
	if err != nil {
		return
	}
	fileNamingFormat := config.FileNameFormat
	if fileNamingFormat == "" {
		fileNamingFormat = "060102.log"
	}
	currentDateStr := time.Now().Format(fileNamingFormat)

	rotatedLogFilename := logsFolderPath + "/" + currentDateStr
	if len(destination) == 1 {
		writer := destination[0]
		writer.initCloseOnContextDone()
		nextDestination, err := os.OpenFile(rotatedLogFilename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		writer.ctx = ctx
		writer.SetOutput(nextDestination)
		return err
	}
	if currentRotatedLogFile != nil {
		currentRotatedLogFile.Close()
	}
	currentRotatedLogFile, err = os.OpenFile(rotatedLogFilename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	rotatedFileLogWriter.initCloseOnContextDone()
	if err != nil {
		return fmt.Errorf("opening or creation of rotated logger failed: %s", err)
	}
	config.removeOldFiles()
	return
}

// InitRotatedfileWriterRotation creates a new rotated file logger.
// Only one (optional) destination should be specified.
// If the (optional) destination is not specified, this rotates the
// packages' internal rotated file writer destination
//
// This should be only called once
func InitRotatedfileWriterRotation(ctx context.Context, config RotatedFilewriterConfig, destination ...*WriterStructWithDefaultDestination) (err error) {
	if len(destination) > 1 {
		return fmt.Errorf("destination of rotated writer is either not set or 1")
	}
	err = initOrRotateWriterDest(ctx, &config, destination...)
	if err != nil {
		return
	}
	schedulerParams := []cron.Option{
		cron.WithParser(cron.NewParser(
			cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow,
		)),
	}
	scheduler := cron.New(schedulerParams...)

	cronRotateFrequency := config.RotateCronFrequency
	if cronRotateFrequency == "" {
		cronRotateFrequency = "0 0 * * *"
	}
	scheduler.AddFunc(cronRotateFrequency, func() {
		initOrRotateWriterDest(ctx, &config, destination...)
	})
	go func() {
		scheduler.Run()
	}()
	go func() {
		<-ctx.Done()
		scheduler.Stop()
	}()
	return
}
